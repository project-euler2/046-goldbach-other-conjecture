import math
import time
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(2,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def smallest_composite():
    i = 9
    while True:
        if is_prime(i) == False:
            prime_under_num = Prime_Number_In_Range(i)
            works = False
            for prime in prime_under_num:
                works = works or math.isqrt(int((i-prime)/2)) ** 2 == int((i-prime)/2)
            if works == False:
                return i
        i+= 2

print(smallest_composite())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )